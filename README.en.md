# wechat-quick

#### Description
wechat-quick是基于cocos creator对原生微信登录和分享功能的一个封装。项目包含2部分，creator模块和原生模块。creator部分是js语言实现，js部分主要功能封装在一个WechatModule.js文件中，主要包含三部分功能：
1.调用oc和java的原生微信登录分享接口；
2.登录和分享的全局回调函数；
3.保存登录token方便下次快捷登录;
原生模块是通过反射和js模块互调。具体登录和分享功能都是实现在原生模块WechatModule.java和WechatModule.mm文件中。
原生模块的全部代码都放在了build-templates。用户可以直接构建编译，自动把大部分代码集成在项目中。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
