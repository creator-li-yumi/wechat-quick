# wechat-quick

#### 介绍
wechat-quick是基于cocos creator对原生微信登录和分享功能的一个封装。项目包含2部分，creator模块和原生模块。creator部分是js语言实现，js部分主要功能封装在一个WechatModule.js文件中，主要包含三部分功能：
1.调用oc和java的原生微信登录分享接口；
2.登录和分享的全局回调函数；
3.保存登录token方便下次快捷登录;
原生模块是通过反射和js模块互调。具体登录和分享功能都是实现在原生模块WechatModule.java和WechatModule.mm文件中。
原生模块的全部代码都放在了build-templates。用户可以直接构建编译，自动把大部分代码集成在项目中。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
